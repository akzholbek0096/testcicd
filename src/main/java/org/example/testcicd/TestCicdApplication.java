package org.example.testcicd;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class })
public class TestCicdApplication {

    public static void main(String[] args) {
        SpringApplication.run(TestCicdApplication.class, args);
    }

}
